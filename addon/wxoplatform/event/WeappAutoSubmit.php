<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+


namespace addon\wxoplatform\event;

use addon\wxoplatform\model\Weapp;

/**
 * 小程序自动提交
 */
class WeappAutoSubmit
{
    /**
     * 执行安装
     */
    public function handle()
    {
        $weapp = new Weapp();
        $weapp->autoSubmitAudit();
    }
}