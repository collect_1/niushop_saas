<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\wxoplatform\model;

use app\model\system\Config as ConfigModel;
use app\model\BaseModel;

/**
 * 微信公众号配置
 * 版本 1.0.4
 */
class Config extends BaseModel
{

    /******************************************************************** 微信开放平台配置 start ****************************************************************************/
    /**
     * 设置开放平台配置
     * @return multitype:string mixed
     */
    public function setOplatformConfig($data, $is_use)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '微信开放平台设置', $is_use, [['site_id', '=', 0], ['app_module', '=', 'admin'], ['config_key', '=', 'OPLATFORM_CONFIG']]);
        return $res;
    }

    /**
     * 获取开放平台配置信息
     * @return multitype:string mixed
     */
    public function getOplatformConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([['site_id', '=', 0], ['app_module', '=', 'admin'], ['config_key', '=', 'OPLATFORM_CONFIG']]);
        return $res;
    }
    /******************************************************************** 微信开放平台配置 end ****************************************************************************/

    /**
     * 设置小程序版本信息
     * @param $data
     * @param $is_use
     * @param int $site_id
     * @return array
     */
    public function setWeappVersion($data, $is_use)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '小程序版本', $is_use, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'WEAPP_VERSION' ] ]);
        return $res;
    }

    /**
     * 获取小程序版本信息
     * @param int $site_id
     * @return array
     */
    public function getWeappVersion()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'WEAPP_VERSION' ] ]);
        return $res;
    }
}