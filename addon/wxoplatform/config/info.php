<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
return [
    'name' => 'wxoplatform',
    'title' => '微信开放平台',
    'description' => '微信开放平台',
    'type' => 'system', //插件类型  system :系统插件(自动安装), promotion:扩展营销插件  tool:工具插件
    'status' => 1,
    'author' => '',
    'version' => '1.1.5',
    'version_no' => '202111080001',
    'content' => '',
];