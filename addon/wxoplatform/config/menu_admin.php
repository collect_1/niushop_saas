<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
return [
    [
        'name' => 'OPLATFORM_CONFIG',
        'title' => '微信开放平台配置',
        'url' => 'wxoplatform://admin/oplatform/config',
        'parent' => 'BASE_CONFIG',
        'is_show' => 1,
        'is_control' => 1,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 6,
        'child_list' => [
            [
                'name' => 'WXOPLATFORM_CONFIG',
                'title' => '配置',
                'url' => 'wxoplatform://admin/oplatform/config',
                'is_show' => 1,
                'sort' => 1,
            ],
            [
                'name' => 'WXOPLATFORM_WEAPP_SOURCE_CODE',
                'title' => '小程序源码',
                'url' => 'wxoplatform://admin/oplatform/sourcecode',
                'is_show' => 1,
                'sort' => 2,
            ],
            [
                'name' => 'WXOPLATFORM_DRAFT',
                'title' => '草稿箱',
                'url' => 'wxoplatform://admin/oplatform/draft',
                'is_show' => 1,
                'sort' => 3,
            ],
            [
                'name' => 'WXOPLATFORM_TEMPLATE',
                'title' => '模板库',
                'url' => 'wxoplatform://admin/oplatform/template',
                'is_show' => 1,
                'sort' => 4,
            ]
        ]
    ],
        
];