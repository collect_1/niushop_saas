<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+
return [
    [
        'name' => 'WECHAT_SETTING_AUTH',
        'title' => '公众号授权',
        'url' => 'wxoplatform://shop/oplatform/wechatSettled',
        'is_show' => 0,
        'parent' => 'WECHAT_ROOT',
    ],
    [
        'name' => 'WECHAT_SETTING_AUTH_CONFIG',
        'title' => '公众号授权配置',
        'url' => 'wxoplatform://shop/oplatform/wechat',
        'is_show' => 0,
        'parent' => 'WECHAT_ROOT',
    ],
    [
        'name' => 'AUTH_MSG',
        'title' => '授权通知',
        'url' => 'wxoplatform://shop/oplatform/callback',
        'is_show' => 0,
        'parent' => 'CHANNEL_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_AUTH_CONFIG',
        'title' => '小程序授权配置',
        'url' => 'wxoplatform://shop/oplatform/weapp',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_AUTH',
        'title' => '小程序授权',
        'url' => 'wxoplatform://shop/oplatform/weappSettled',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_TO_EXAMINE',
        'title' => '小程序审核',
        'url' => 'wxoplatform://shop/weapp/plugin',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_EXAMINE_RECORD',
        'title' => '审核记录',
        'url' => 'wxoplatform://shop/weapp/record',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_EXPERIENCER',
        'title' => '体验者管理',
        'url' => 'wxoplatform://shop/weapp/experiencer',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_BINDTESTUSER',
        'title' => '绑定体验者',
        'url' => 'wxoplatform://shop/weapp/bindTestUser',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_UNBINDTESTUSER',
        'title' => '解绑体验者',
        'url' => 'wxoplatform://shop/weapp/unbindTestUser',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_COMMIT',
        'title' => '微信小程序代码上传',
        'url' => 'wxoplatform://shop/weapp/commit',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_RELEASE',
        'title' => '发布',
        'url' => 'wxoplatform://shop/weapp/release',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
    [
        'name' => 'WEAPP_SETTING_UNDOCODEAUDIT',
        'title' => '撤回审核',
        'url' => 'wxoplatform://shop/weapp/undocodeaudit',
        'is_show' => 0,
        'parent' => 'WEAPP_ROOT',
    ],
];