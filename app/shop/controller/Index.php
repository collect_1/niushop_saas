<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\system\Addon;
use app\model\system\SiteGroup;
use app\model\shop\Shop as ShopModel;
use app\model\system\Stat;
use app\model\system\SysUser;
use Carbon\Carbon;
use app\model\system\Promotion as PrmotionModel;
use app\model\web\Help as HelpModel;
use app\model\goods\Goods as GoodsModel;
use app\model\web\Notice as NoticeModel;
use app\model\system\SysOrder as SysOrderModel;
use app\model\member\Member as MemberModel;
use app\model\order\OrderCommon;
use app\model\order\OrderRefund as OrderRefundModel;
use addon\fenxiao\model\FenxiaoWithdraw;
use addon\fenxiao\model\FenxiaoApply;


class Index extends BaseShop
{

    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
        $shop_model = new ShopModel();
        $shop_info = $this->shop_info;
        $time = time();
        if ($shop_info[ 'expire_time' ] == 0) {
            $shop_status = 1;//正常
        } elseif ($shop_info[ 'expire_time' ] > $time) {
            $cha = $shop_info[ 'expire_time' ] - $time;
            $date = ceil(( $cha / 86400 ));
            if ($date < 30) {
                $shop_status = 2;//即将到期
                $this->assign('expire_date', $date);
            } else {
                $shop_status = 1;//正常
            }
        } else {
            $shop_status = -1;//关闭
        }
        $this->assign('shop_status', $shop_status);

        //基础统计信息
        $stat_shop_model = new Stat();
        $today = Carbon::now();
        $yesterday = Carbon::yesterday();
        $stat_today = $stat_shop_model->getStatShop($this->site_id, $today->year, $today->month, $today->day);
        $stat_yesterday = $stat_shop_model->getStatShop($this->site_id, $yesterday->year, $yesterday->month, $yesterday->day);
        $this->assign("stat_day", $stat_today[ 'data' ]);
        $this->assign("stat_yesterday", $stat_yesterday[ 'data' ]);
        $this->assign("today", $today);

        //获取总数
        $shop_stat_sum = $stat_shop_model->getShopStatSum($this->site_id);
        $goods_model = new GoodsModel();
        $goods_sum = $goods_model->getGoodsTotalCount([ 'site_id' => $this->site_id, 'is_delete' => 0 ]);
        $shop_stat_sum[ 'data' ][ 'goods_count' ] = $goods_sum[ 'data' ];
        $this->assign('shop_stat_sum', $shop_stat_sum[ 'data' ]);

        //会员总数
        $member_model = new MemberModel();
        $member_count = $member_model->getMemberCount([ [ 'site_id', '=', $this->site_id ] ]);
        $this->assign('member_count', $member_count[ 'data' ]);

        //日同比
        $day_rate[ 'order_pay_count' ] = diff_rate($stat_today[ 'data' ][ 'order_pay_count' ], $stat_yesterday[ 'data' ][ 'order_pay_count' ]);
        $day_rate[ 'order_total' ] = diff_rate($stat_today[ 'data' ][ 'order_total' ], $stat_yesterday[ 'data' ][ 'order_total' ]);
        $day_rate[ 'collect_goods' ] = diff_rate($stat_today[ 'data' ][ 'collect_goods' ], $stat_yesterday[ 'data' ][ 'collect_goods' ]);
        $day_rate[ 'visit_count' ] = diff_rate($stat_today[ 'data' ][ 'visit_count' ], $stat_yesterday[ 'data' ][ 'visit_count' ]);
        $day_rate[ 'member_count' ] = diff_rate($stat_today[ 'data' ][ 'member_count' ], $stat_yesterday[ 'data' ][ 'member_count' ]);

        $this->assign('day_rate', $day_rate);
        //周同比
        $begin_last_week = mktime(0,0,0,date('m'),date('d')-date('w')+1-7,date('Y'));
        $end_last_week = mktime(23,59,59,date('m'),date('d')-date('w')+7-7,date('Y'));

        $begin_week = mktime(0,0,0,date("m"),date("d")-date("w")+1,date("Y"));
        $end_week = mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));

        $stat_last_week = $stat_shop_model->getStatShopByCondition($this->site_id, $begin_last_week, $end_last_week);

        $stat_week = $stat_shop_model->getStatShopByCondition($this->site_id, $begin_week, $end_week);
        $week_rate = [];
        $week_rate[ 'order_pay_count' ] = diff_rate($stat_week[ 'data' ][ 'order_pay_count' ], $stat_last_week[ 'data' ][ 'order_pay_count' ]);
        $week_rate[ 'order_total' ] = diff_rate($stat_week[ 'data' ][ 'order_total' ], $stat_last_week[ 'data' ][ 'order_total' ]);
        $week_rate[ 'collect_goods' ] = diff_rate($stat_week[ 'data' ][ 'collect_goods' ], $stat_last_week[ 'data' ][ 'collect_goods' ]);
        $week_rate[ 'visit_count' ] = diff_rate($stat_week[ 'data' ][ 'visit_count' ], $stat_last_week[ 'data' ][ 'visit_count' ]);
        $week_rate[ 'member_count' ] = diff_rate($stat_week[ 'data' ][ 'member_count' ], $stat_last_week[ 'data' ][ 'member_count' ]);

        $this->assign('week_rate', $week_rate);

        //订单数
        $order = new OrderCommon();
        $waitpay = $order->getOrderCount([ ['order_status','=', 0],['site_id','=',$this->site_id], ['is_delete','=',0]]);
        $waitsend = $order->getOrderCount([ ['order_status','=', 1],['site_id','=',$this->site_id], ['is_delete','=',0]]);

        $order_refund_model = new OrderRefundModel();
        $refund_num = $order_refund_model->getRefundOrderGoodsCount([
            ["site_id", "=", $this->site_id],
            ["refund_status", "not in", [0, 3]]
        ]);
        $goods_stock_alarm = $goods_model->getGoodsStockAlarm($this->site_id);


        //营销活动
        $promotion_model = new PrmotionModel();
        $promotions = $promotion_model->getSitePromotions($this->site_id);

        $this->assign("promotion", $promotions);

        //网站公告
        $notice_model = new NoticeModel();
        $notice_list = $notice_model->getNoticePageList([ [ 'site_id', '=', 0 ] ], 1, 5, 'is_top desc,create_time desc', 'id,title');
        $this->assign('notice_list', $notice_list[ 'data' ][ 'list' ]);

        //入驻帮助
        $help_model = new HelpModel();
        $help_condition = [
            [ 'site_id', '=', 0 ]
        ];
        $order = 'create_time desc';
        $help_list = $help_model->getHelpPageList($help_condition, 1, 5, $order);
        $this->assign("help_list", $help_list[ 'data' ][ 'list' ]);

        //分销插件是否存在
        $is_fenxiao = addon_is_exit('fenxiao', $this->site_id);
        $this->assign('is_fenxiao', $is_fenxiao);

        $num_data = [
            'waitpay' => $waitpay['data'],
            'waitsend' => $waitsend['data'],
            'refund' => $refund_num['data'],
            'goods_stock_alarm' => is_array($goods_stock_alarm['data']) ? count($goods_stock_alarm['data']) : 0
        ];

        if($is_fenxiao){
            //提现待审核
            $fenxiao_model = new FenxiaoWithdraw();
            $withdraw_count = $fenxiao_model->getFenxiaoWithdrawCount([ ['site_id','=',$this->site_id], ['status','=',1]], 'id');
            $num_data['withdraw_count'] = $withdraw_count['data'];

            //分销商申请
            $fenxiao_apply_model = new FenxiaoApply();
            $fenxiao_count = $fenxiao_apply_model->getFenxiaoApplyCount([ ['site_id','=',$this->site_id], ['status','=',1]], 'apply_id');
            $num_data['apply_count'] = $fenxiao_count['data'];
        }

        $this->assign('num_data', $num_data);

        //近十天的订单数以及销售金额
        $date_day = getweeks();
        $order_total = '';
        $order_pay_count = '';
        foreach ($date_day as $k => $day) {
            $dayarr = explode('-', $day);
            $stat_day[ $k ] = $stat_shop_model->getStatShop($this->site_id, $dayarr[ 0 ], $dayarr[ 1 ], $dayarr[ 2 ]);
            $order_total .= $stat_day[ $k ][ 'data' ][ 'order_total' ] . ',';
            $order_pay_count .= $stat_day[ $k ][ 'data' ][ 'order_pay_count' ] . ',';
        }
        $ten_day[ 'order_total' ] = explode(',', substr($order_total, 0, strlen($order_total) - 1));
        $ten_day[ 'order_pay_count' ] = explode(',', substr($order_pay_count, 0, strlen($order_pay_count) - 1));
        $this->assign('ten_day', $ten_day);

        //店铺头像
        $shop_info = $shop_model->getShopInfo([ ['site_id','=',$this->site_id] ], 'avatar');
        $avatar = $shop_info['data']['avatar'];
        $this->assign('avatar', $avatar);

        return $this->fetch("index/index");
    }

    /**
     * 续费
     */
    public function renewExpireTime()
    {
        if (request()->isAjax()) {
            $group_id = input('group_id', 0);//购买用户套餐数量
            $num = input('num', 0);//购买用户套餐数量
            $sms_package_id = input('sms_package_id', 0);//短信套餐id
            $sms_package_num = input('sms_package_num', 1);//购买短信套餐数量

            //获取系统用户信息
            $sys_model = new SysUser();
            $sys_info = $sys_model->getUserInfo([ [ 'sys_uid', '=', $this->user_info[ 'sys_uid' ] ] ]);
            $buyer_info = array (
                'buyer_uid' => $sys_info[ 'data' ][ 'sys_uid' ],
                'buyer_name' => $sys_info[ 'data' ][ 'username' ],
            );
            $data = array (
                'group_id' => $group_id,
                'site_name' => $this->shop_info[ 'site_name' ],
                'site_id' => $this->site_id,
                'group_num' => $num,
                'buyer_info' => $buyer_info,
                'sms_package_id' => $sms_package_id,
                'sms_package_num' => $sms_package_num,
            );
            $sys_order_model = new SysOrderModel();
            $res = $sys_order_model->orderCreate($data);
            return $res;

        } else {

            $addon = new Addon();
            $install_result = $addon->getAddonList([ [ "type", "<>", "system" ] ], "name,title");
            $install = $install_result[ "data" ];
            $install_list = array_column($install, "title", "name");
            $sys_user_group_model = new SiteGroup();
            $list = $sys_user_group_model->getSiteGroupList([ [ 'status', '=', 1 ] ]);
            if (!empty($list[ "data" ])) {
                foreach ($list[ "data" ] as $k => $v) {
                    $temp_array = explode(",", $v[ 'addon_array' ]);
                    $exist_addon_array = [];
                    $noexist_addon_array = [];
                    foreach ($install_list as $temp_k => $temp_v) {
                        if (in_array($temp_k, $temp_array)) {
                            $exist_addon_array[] = $temp_v;
                        } else {
                            $noexist_addon_array[] = $temp_v;
                        }
                    }
                    $list[ "data" ][ $k ][ "exist_addon_list" ] = $exist_addon_array;
                    $list[ "data" ][ $k ][ "noexist_addon_list" ] = $noexist_addon_array;
                }
            }
            $this->assign('addon_list', $list[ 'data' ]);

            $this->assign('menu_info', [ 'title' => '套餐续费' ]);

            $shop_info = $this->shop_info;
            $time = time();
            if ($shop_info[ 'expire_time' ] == 0) {
                $shop_status = 1;//正常
            } elseif ($shop_info[ 'expire_time' ] > $time) {
                $cha = $shop_info[ 'expire_time' ] - $time;
                $date = ceil(( $cha / 86400 ));
                if ($date < 30) {
                    $shop_status = 2;//即将到期
                    $this->assign('expire_date', $date);
                } else {
                    $shop_status = 1;//正常
                }
            } else {
                $shop_status = -1;//关闭
            }
            $this->assign('shop_status', $shop_status);

            return $this->fetch('index/renew_expire_time');
        }
    }

}