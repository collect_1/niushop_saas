<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\system\SysOrder as SysOrderModel;


/**
 * 订单 控制器
 */
class Sysorder extends BaseShop
{

	/**
	 * 列表
	 */
	public function lists()
	{
		$order_type = input('order_type', '');
		$sys_order_model = new SysOrderModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			
			$condition[] = ['site_id','=',$this->site_id];
			$condition[] = [ 'is_delete', '=', 0 ];
			$search_type = input('search_type', '');
			$search_text = input('search_text', '');
			if (!empty($search_text)) {
				$condition[] = [ $search_type, 'LIKE', '%' . $search_text . '%' ];
			}
			
			if ($order_type) {
				$condition[] = [ 'order_type', '=', $order_type ];
			}
			
			$order_status = input('order_status', '');
			if ($order_status !== '') {
				$condition[] = [ 'order_status', '=', $order_status ];
			}
			
			$pay_type = input('pay_type', '');
			if ($pay_type) {
				$condition[] = [ 'pay_type', '=', $pay_type ];
			}
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && !$end_time) {
				$condition[] = [ 'create_time', '>=', date_to_time($start_time) ];
			} elseif (!$start_time && $end_time) {
				$condition[] = [ 'create_time', '<=', date_to_time($end_time) ];
			} elseif ($start_time && $end_time) {
				$condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
			}
			
			$res = $sys_order_model->getSysOrderPageList($condition, $page, $page_size);
			return $res;
			
		} else {
			//订单类型
			$order_type_list = $sys_order_model->getOrderType();
			$this->assign('order_type_list', $order_type_list);
			//支付方式
			$pay_type_list = $sys_order_model->getPayType();
			$this->assign('pay_type_list', $pay_type_list);
			//订单状态
			$order_status = $sys_order_model->getOrderStatus();
			$this->assign('order_status', $order_status);
			$this->assign('order_type', $order_type);
			return $this->fetch('sysorder/lists');
		}
		
	}

	public function confirm(){
        $out_trade_no = input("out_trade_no", '');
        $sys_order_model = new SysOrderModel();
        if (request()->isAjax()) {

            $data = [
                'paying_money_certificate' => input('paying_money_certificate', ''),
                'paying_money_certificate_explain' => input('paying_money_certificate_explain', '')
            ];
            $condition[] = [ 'out_trade_no', '=', $out_trade_no ];
            $condition[] = [ 'pay_status', '=', 0 ];
            return $sys_order_model->applyOfflinePay($condition, $data);

        }
    }
	
	/**
	 * 详情
	 */
	public function detail()
	{
        $order_id = input('order_id', 0);
        $order_model = new SysOrderModel();
        $condition = array(
            [ 'order_id', '=', $order_id ],
            [ 'buyer_uid', '=', $this->user_info['sys_uid']],
        );
        $detail_result = $order_model->getOrderInfo($condition);
        $detail = $detail_result["data"];
        $this->assign("detail", $detail);
		return $this->fetch('sysorder/detail');
	}

    /**
     * 关闭订单
     */
    public function close()
    {
        if (request()->isAjax()) {
            $order_id = input('order_id', 0);
            $order_model = new SysOrderModel();
            $condition = array(
                [ 'order_id', '=', $order_id ],
                [ 'buyer_uid', '=', $this->user_info['sys_uid']],
                [ 'order_status', 'not in', [ -1, 1 ] ],
            );
            $result = $order_model->orderClose($condition);
            return $result;
        }
    }

    /**
     * 删除订单
     * @return array
     */
    public function delete()
    {
        if (request()->isAjax()) {
            $order_id = input('order_id', 0);
            $order_model = new SysOrderModel();
            $condition = array(
                [ 'order_id', '=', $order_id ],
                [ 'buyer_uid', '=', $this->user_info['sys_uid']],
                [ 'order_status', '=', -1 ],
            );
            $result = $order_model->orderDelete($condition);
            return $result;
        }
    }

}