<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\system\Group;
use app\model\system\Menu;
use app\model\system\Promotion as PromotionModel;
use app\model\system\Site as SiteModel;

/**
 * 营销
 * Class Promotion
 * @package app\shop\controller
 */
class Promotion extends BaseShop
{

    public function __construct()
    {
        //执行父类构造函数
        parent::__construct();
    }

    /**
     * 营销中心
     * @return mixed
     */
    public function index()
    {
        $promotion_model = new PromotionModel();
        $promotions = $promotion_model->getSitePromotions($this->site_id);
        // 查询站点购买的插件
        $site_addon = array_column($this->site_addon, 'name');
        array_push($site_addon, '');
        //获取菜单权限
        $menu_model = new Menu();
        //处理选中数据
        $checked_array = explode(',',$this->group_info['menu_array'] );
        foreach ($promotions as $key => &$val) {
            if($this->user_info['is_admin']!='1'){
                $menu_info = $menu_model->getMenuInfoByUrl( $val['url'], $this->app_module,'','level asc');
                if (in_array($menu_info['data'][ 'name' ], $checked_array)) {
                    $promotions[ $key ][ 'is_has_menu' ] = 1;
                } else {
                    $promotions[ $key ][ 'is_has_menu' ] = 0;
                }
            }else{
                $promotions[ $key ][ 'is_has_menu' ] = 1;
            }

        }
        $this->assign("promotion", $promotions);
        return $this->fetch("promotion/index");
    }

    /**
     * 平台营销
     * @return mixed
     */
    public function platform()
    {
        $promotion_model = new PromotionModel();
        $promotions = $promotion_model->getPromotions();
        $site_model = new SiteModel();
        $site_info = $site_model->getSiteInfo([ 'site_id' => $this->site_id ], 'site_addon');
        $addon_array = explode(',', $site_info[ 'data' ][ 'site_addon' ]);
        foreach ($promotions[ 'shop' ] as $key => $promotion) {
            if (!in_array($promotion[ 'name' ], $addon_array) && empty($promotion[ 'is_developing' ])) {
                unset($promotions[ 'shop' ][ $key ]);
            }
        }
        $this->assign("promotion", $promotions[ 'shop' ]);
        return $this->fetch("promotion/platform");
    }

    /**
     * 会员营销
     * @return mixed
     */
    public function member()
    {
        $promotion_model = new PromotionModel();
        $promotions = $promotion_model->getSitePromotions($this->site_id);
        // 查询站点购买的插件
        $site_addon = array_column($this->site_addon, 'name');
        array_push($site_addon, '');
        //获取菜单权限
        $menu_model = new Menu();
        //处理选中数据
        $checked_array = explode(',',$this->group_info['menu_array'] );
        foreach ($promotions as $key => &$val) {
            if($this->user_info['is_admin']!='1'){
                $menu_info = $menu_model->getMenuInfoByUrl( $val['url'], $this->app_module,'','level asc');
                if (in_array($menu_info['data'][ 'name' ], $checked_array)) {
                    $promotions[ $key ][ 'is_has_menu' ] = 1;
                } else {
                    $promotions[ $key ][ 'is_has_menu' ] = 0;
                }
            }else{
                $promotions[ $key ][ 'is_has_menu' ] = 1;
            }

        }
        $this->assign("promotion", $promotions);
        return $this->fetch("promotion/member");
    }

    /**
     * 营销工具
     * @return mixed
     */
    public function tool()
    {
        $promotion_model = new PromotionModel();
        $promotions = $promotion_model->getPromotions();
        $site_model = new SiteModel();
        $site_info = $site_model->getSiteInfo([ 'site_id' => $this->site_id ], 'site_addon');
        $addon_array = explode(',', $site_info[ 'data' ][ 'site_addon' ]);
        foreach ($promotions[ 'shop' ] as $key => $promotion) {
            if (!in_array($promotion[ 'name' ], $addon_array) && empty($promotion[ 'is_developing' ])) {
                unset($promotions[ 'shop' ][ $key ]);
            }
        }
        // 查询站点购买的插件
        $site_addon = array_column($this->site_addon, 'name');
        array_push($site_addon, '');
        //获取菜单权限
        $menu_model = new Menu();
        //处理选中数据
        $checked_array = explode(',',$this->group_info['menu_array'] );
        foreach ($promotions['shop'] as $key => &$val) {
            if($this->user_info['is_admin']!='1'){
                $menu_info = $menu_model->getMenuInfoByUrl( $val['url'], $this->app_module,'','level asc');
                if (in_array($menu_info['data'][ 'name' ], $checked_array)) {
                    $promotions['shop'][ $key ][ 'is_has_menu' ] = 1;
                } else {
                    $promotions['shop'][ $key ][ 'is_has_menu' ] = 0;
                }
            }else{
                $promotions['shop'][ $key ][ 'is_has_menu' ] = 1;
            }

        }
        $this->assign("promotion", $promotions[ 'shop' ]);
        return $this->fetch("promotion/tool");
    }
}