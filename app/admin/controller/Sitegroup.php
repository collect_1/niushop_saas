<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\system\Addon;
use app\model\system\SiteConfig;
use app\model\system\SiteGroup as SiteGroupModel;

/**
 * 权限组 控制器
 * 版本 1.0.6
 */
class Sitegroup extends BaseAdmin
{
	/**
	 * 列表
	 */
	public function lists()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$condition = [];
			$status = input('status', 'all');
			$search_keys = input('search_keys', "");
			if (!empty($search_keys)) {
				$condition[] = [ 'group_name', 'like', '%' . $search_keys . '%' ];
			}
			if ($status != 'all' && $status != '') {
				$condition['status'] = [ 'status', '=', $status ];
			}
			$user_model = new SiteGroupModel();
			$list = $user_model->getSiteGroupPageList($condition, $page, $page_size, "create_time desc");
			return $list;
		}
		return $this->fetch('sitegroup/lists');
	}
	
	/**
	 * 添加
	 * @return mixed
	 */
	public function add()
	{
		if (request()->isAjax()) {
		    $site_type = input('site_type', 'b2c');
			$sys_user_group_data = [
				'type' => 'group',
				'group_name' => input('group_name', ''),
				'image' => input('group_image', ''),
				'fee' => input('fee', 0),
				'unit' => input('unit', 'week'),
				'status' => input('status', 0),
				'desc' => input('desc', ''),
				'addon_array' => input('addon_array', ''),
				'sms_num' => input('sms_num', 0)
			];
			$sys_user_group_model = new SiteGroupModel();
			$res = $sys_user_group_model->addSiteGroup($sys_user_group_data);
			return $res;
		} else {
			$addon = new Addon();
			$install = $addon->getAddonList([ [ "type", "<>", "system" ] ]);
			$this->assign("addons", $install['data']);
			return $this->fetch('sitegroup/add');
		}
	}
	
	/**
	 * 编辑
	 * @return mixed
	 */
	public function edit()
	{
		$group_id = input('group_id', 0);
        $site_type = input('site_type', 'b2c');
		$sys_user_group_model = new SiteGroupModel();
		if (request()->isAjax()) {
			$sys_user_group_data = [
				'type' => 'group',
				'group_name' => input('group_name', ''),
				'image' => input('group_image', ''),
				'fee' => input('fee', 0),
				'unit' => input('unit', 'week'),
				'status' => input('status', 0),
				'desc' => input('desc', ''),
				'addon_array' => input('addon_array', ''),
				'sms_num' => input('sms_num', 0)
			];
			$sys_user_group_model = new SiteGroupModel();
			$res = $sys_user_group_model->editSiteGroup($sys_user_group_data, [ [ 'group_id', '=', $group_id ] ]);
			return $res;
		} else {
			$group_info = $sys_user_group_model->getSiteGroupDetail([ [ 'group_id', '=', $group_id ] ]);
			$addon = new Addon();
			$install = $addon->getAddonList([ [ "type", "<>", "system" ] ]);
			$addon_array = $group_info['data']['addon_array'];
			$addon_array = explode(',', $addon_array);
			foreach ($install['data'] as &$v) {
				if (in_array($v['name'], $addon_array)) {
					$v['is_checked'] = 1;
				} else {
					$v['is_checked'] = 0;
				}
			}
			$this->assign('addons', $install['data']);
			$this->assign('info', $group_info['data']);
			return $this->fetch('sitegroup/edit');
		}
	}
	
	/**
	 * 删除
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$group_id = input('group_id', 0);
			$sys_user_group_model = new SiteGroupModel();
			$res = $sys_user_group_model->deleteSiteGroup($group_id);
			return $res;
		}
	}
	
	/**
	 * 店铺设置
	 */
	public function config()
	{
		$config = new SiteConfig();
		if (request()->isAjax()) {
			
			$data = [
				'status' => input('status', '0'),
				'day' => input('day', '7'),
				'group_id' => input('group_id', '0'),
				'site_num' => input('site_num', '10'),
                'sms_num' => input('sms_num', '30'),
			];
			return $config->setOpenSiteConfig($data);
		} else {
			
			$res = $config->getOpenSiteConfig();
			$this->assign('config_info', $res['data']['value']);
			
			$group_model = new SiteGroupModel();
			$group_list = $group_model->getSiteGroupList([ [ 'status', '=', 1 ] ], 'group_id,group_name');
			$this->assign('group_list', $group_list['data']);
			
			return $this->fetch('sitegroup/config');
		}
		
	}
}