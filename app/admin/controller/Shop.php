<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\shop\Shop as ShopModel;
use app\model\system\Addon;
use app\model\system\Promotion as PrmotionModel;
use app\model\system\Site as SiteModel;
use phpoffice\phpexcel\Classes\PHPExcel;
use phpoffice\phpexcel\Classes\PHPExcel\Writer\Excel2007;
use think\facade\Cache;
use app\model\system\SiteGroup as SiteGroupModel;
use think\facade\Db;

/**
 * 商家管理 控制器
 * 版本 1.0.6
 */
class Shop extends BaseAdmin
{
	
	/******************************* 商家列表及相关操作 ***************************/
	
	/**
	 * 商家列表
	 */
	public function lists()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$search_text = input('search_text', '');
			$username = input('username', '');
			
			$condition = [];
			if ($search_text) {
				$condition[] = [ 'site_name', 'like', '%' . $search_text . '%' ];
			}
			if ($username) {
				$condition[] = [ 'username', 'like', '%' . $username . '%' ];
			}
			
			$order = 'site_id desc';
			$field = '*';
			
			$site_model = new SiteModel();
			return $site_model->getSitePageList($condition, $page, $page_size, $order, $field);
			
		} else {
			
			$cert = Cache::get("niushop_cert");
			$type = $cert['type'];
			$this->assign("business_type", $type);
			return $this->fetch('shop/lists');
		}
	}
	
	/**
	 * 商家详情
	 */
	public function shopDetail()
	{
		$site_id = input('site_id', 0);
		$site_id = $site_id ? : request()->siteid();
		
		$site_model = new SiteModel();
		$site_info = $site_model->getSiteInfo([ [ 'site_id', '=', $site_id ] ], '*');
		
		$addon_array = explode(',', $site_info['data']['site_addon']);
		
		$promotion_model = new PrmotionModel();
		$promotions = $promotion_model->getPromotions();
		$promotions = $promotions['shop'];
		foreach ($promotions as $key => &$promotion) {
			if (!empty($promotion['is_developing'])) {
				unset($promotions[ $key ]);
				continue;
			}
			$promotion['is_checked'] = 0;
			if (in_array($promotion['name'], $addon_array)) {
				$promotion['is_checked'] = 1;
			} else {
				unset($promotions[ $key ]);
			}
		}
		
		$addon = new Addon();
		$site_addon = $addon->getAddonList([ [ "type", "<>", "system" ], ['name', 'in', $site_info['data']['site_addon'] ] ]);
		$this->assign('promotions', $site_addon['data']);
		
		// $this->assign('promotions', $promotions);
		$this->assign('shop_info', $site_info['data']);
		
		return $this->fetch('shop/shop_detail');
	}
	
	/**
	 * 商家添加
	 */
	public function addShop()
	{
		$promotion_model = new PrmotionModel();
		$promotions = $promotion_model->getPromotions();
		if (request()->isAjax()) {
			//店铺信息
			$shop_data = [
				'site_name' => input('site_name', ''),//店铺名称
				'site_domain' => input('site_domain', ''),
				'site_addon' => input('addons', ''),
				'expire_time' => 0,
				'sys_uid' => $this->sys_uid
			];
			
			$shop_model = new SiteModel();
			$this->addLog("添加商家:" . $shop_data['site_name']);
			return $shop_model->addSite($shop_data);
			
		} else {
			//获取可应用的模块
			foreach ($promotions['shop'] as $key => $promotion) {
				if (!empty($promotion['is_developing'])) {
					unset($promotions['shop'][ $key ]);
					continue;
				}
			}
			$this->assign("promotions", $promotions['shop']);
			return $this->fetch('shop/add_shop');
		}
	}
	
	/**
	 *  删除店铺
	 */
	public function delete()
	{
		$site_id = input('site_id', 0);
		$site_id = $site_id ? : request()->siteid();
		
		$shop_model = new ShopModel();
		return $shop_model->deleteShop($site_id);
	}

    /**
     * 修改商城配置
     */
    public function edit(){
        $site_id = input('site_id', 0);
        $site_id = $site_id ?: request()->siteid();

        if (request()->isAjax()) {
            $site_data = [
                'site_domain' => input('site_domain', ''),
                'site_addon' => input('site_addon', ''),
                'group_id' => input('group_id', ''),
                'group_name' => input('group_name', ''),
                'expire_time' => input('expire_time', 0)
            ];
            if (!empty($site_data['expire_time'])) $site_data['expire_time'] = strtotime($site_data['expire_time']);
            $site_model = new SiteModel();
            return $site_model->editSiteShop($site_data, [['site_id', '=', $site_id]]);
        } else {
            $site_group_model = new SiteGroupModel();
            $site_model = new SiteModel();

            $fields = 'a.*';
            $shop_info = $site_model->getSiteShopInfo([['a.site_id', '=', $site_id]], $fields);
            $this->assign('shop_info', $shop_info['data']);

            $where = [];
            $where[] = ['','exp',Db::raw(join(' OR ',["group_id = ".$shop_info['data']['group_id'],"status = 1"]))];

            $site_group = $site_group_model->getSiteGroupList($where);

            $this->assign('site_group', $site_group['data']);

            $addon = new Addon();
            $site_addon = $addon->getAddonList([ [ "type", "<>", "system" ], ['name', 'in', $shop_info['data']['site_addon'] ] ]);
            $this->assign('site_addon', $site_addon['data']);

            return $this->fetch('shop/edit_shop');
        }
    }

    /**
     * 获取插件
     * @return array|mixed
     */
    public function getAddonList(){
      if (request()->isAjax()) {
          $site_addon = input('site_addon', '');
          $addon = new Addon();
          $site_addon = $addon->getAddonList([ [ "type", "<>", "system" ], ['name', 'in', $site_addon ] ]);
          return $site_addon;
      }
  	}

    public function exportshop(){
        $search_text = input('search_text', '');
        $username = input('username', '');

        $condition = [];
        if ($search_text) {
            $condition[] = [ 'site_name', 'like', '%' . $search_text . '%' ];
        }
        if ($username) {
            $condition[] = [ 'username', 'like', '%' . $username . '%' ];
        }

        $order = 'site_id desc';
        $field = '*';

        $site_model = new SiteModel();
        $list = $site_model->getSitePageList($condition, 1, 0, $order, $field);

        // 实例化excel
        $phpExcel = new \PHPExcel();

        $phpExcel->getProperties()->setTitle("店铺信息");
        $phpExcel->getProperties()->setSubject("店铺信息");
        //单独添加列名称
        $phpExcel->setActiveSheetIndex(0);
        $phpExcel->getActiveSheet()->setCellValue('A1', '店铺名称');//可以指定位置
        $phpExcel->getActiveSheet()->setCellValue('B1', '所属用户');
        $phpExcel->getActiveSheet()->setCellValue('C1', '店铺套餐');
        $phpExcel->getActiveSheet()->setCellValue('D1', '开店时间');
        $phpExcel->getActiveSheet()->setCellValue('E1', '到期时间');
        $phpExcel->getActiveSheet()->setCellValue('F1', '是否试用');
        $phpExcel->getActiveSheet()->setCellValue('G1', '状态');
        //循环添加数据（根据自己的逻辑）
        $sex = [ '保密', '男', '女' ];

        foreach ($list[ 'data' ]['list'] as $k => $v) {
            $i = $k + 2;
            $is_try = $v[ 'expire_time' ] == 1 ? '是' : '否';
            $expire_time = $v[ 'expire_time' ] == "" ? "--" : date('Y-m-d H:i:s', $v[ 'expire_time' ]);
            $phpExcel->getActiveSheet()->setCellValue('A' . $i, $v[ 'site_name' ]);
            $phpExcel->getActiveSheet()->setCellValue('B' . $i, $v[ 'username' ]);
            $phpExcel->getActiveSheet()->setCellValue('C' . $i, $v[ 'group_name' ]);
            $phpExcel->getActiveSheet()->setCellValue('D' . $i, date('Y-m-d H:i:s', $v[ 'create_time' ]));
            $phpExcel->getActiveSheet()->setCellValue('E' . $i, $expire_time);
            $phpExcel->getActiveSheet()->setCellValue('F' . $i, $is_try);
            $phpExcel->getActiveSheet()->setCellValue('G' . $i, $v[ 'status' ]);
        }

        // 重命名工作sheet
        $phpExcel->getActiveSheet()->setTitle('店铺信息');
        // 设置第一个sheet为工作的sheet
        $phpExcel->setActiveSheetIndex(0);
        // 保存Excel 2007格式文件，保存路径为当前路径，名字为export.xlsx
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $file = date('Y年m月d日-店铺信息表', time()) . '.xlsx';
        $objWriter->save($file);

        header("Content-type:application/octet-stream");

        $filename = basename($file);
        header("Content-Disposition:attachment;filename = " . $filename);
        header("Accept-ranges:bytes");
        header("Accept-length:" . filesize($file));
        readfile($file);
        unlink($file);
        exit;
    }
}