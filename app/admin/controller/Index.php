<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\system\Config;
use app\model\system\Site;
use app\model\system\SysOrderStat;
use app\model\system\Web as WebModel;
use app\model\system\SysUser as SysUserModel;
use app\model\shop\Shop as ShopModel;
use app\model\system\Upgrade as UpgradeModel;

/**
 * 首页 控制器
 * 版本 1.0.5
 */
class Index extends BaseAdmin
{
	
	/**
	 * 首页
	 */
	public function index()
	{
		$config_model = new Config();
		$system_config = $config_model->getSystemConfig();
		$this->assign('sys_version_no', SYS_VERSION_NO);
		$this->assign('sys_version_name', SYS_VERSION_NAME);
		$this->assign('sys_version', SYS_VERSION);
		$this->assign('sys_release', SYS_RELEASE);
		$this->assign('system_config', $system_config['data']);

        $sys_order_stat_model = new SysOrderStat();
        $sysuser_model = new SysUserModel();
        $site_model = new Site();

        // 十天内的订单统计
        $date_day = getweeks();
        $ten_day = [];
        foreach ($date_day as $k => $day) {
            $dayarr = explode('-', $day);
            $day_start_time = strtotime($day . ' 00:00:00');
            $day_end_time = strtotime($day . ' 23:59:59');
            $ten_day['site'][] = $sys_order_stat_model->getOrderSum([ ['order_type', '=', 'group'], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]], ['create_time', 'between', [$day_start_time, $day_end_time] ] ], 'order_money')['data'];
            $ten_day['sms'][] = $sys_order_stat_model->getOrderSum([ ['order_type', '=', 'sms'], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]], ['create_time', 'between', [$day_start_time, $day_end_time] ] ], 'order_money')['data'];
        }
        $ten_day['site'] = implode(',', $ten_day['site']);
        $ten_day['sms'] = implode(',', $ten_day['sms']);
        $this->assign('ten_day', $ten_day);

        $today_start_time = strtotime(date('Y-m-d 00:00:00', time()));
        $today_end_time = time();

        $yesterday_start = date_to_time(date("Y-m-d 00:00:00", strtotime("-1 day")));
		$yesterday_end = date_to_time(date("Y-m-d 23:59:59", strtotime("-1 day")));

		$yesterday_data = [
		    'user_count' => $sysuser_model->getSysuserCount([[ 'create_time', 'between', [ $yesterday_start, $yesterday_end ] ]]),
            'site_count' => $site_model->getSiteCount([[ 'create_time', 'between', [ $yesterday_start, $yesterday_end ] ]])['data'],
            'order_count' => $sys_order_stat_model->getOrderCount([ [ 'create_time', 'between', [ $yesterday_start, $yesterday_end ] ], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ])['data'],
            'order_money' => $sys_order_stat_model->getOrderSum([ [ 'create_time', 'between', [ $yesterday_start, $yesterday_end ] ], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ], 'order_money')['data']
        ];

        $today_data = [
            'user_count' => $sysuser_model->getSysuserCount([[ 'create_time', 'between', [ $today_start_time, $today_end_time ] ]]),
            'site_count' => $site_model->getSiteCount([[ 'create_time', 'between', [ $today_start_time, $today_end_time ] ]])['data'],
            'order_count' => $sys_order_stat_model->getOrderCount([ [ 'create_time', 'between', [ $today_start_time, $today_end_time ] ], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ])['data'],
            'order_money' => $sys_order_stat_model->getOrderSum([ [ 'create_time', 'between', [ $today_start_time, $today_end_time ] ], ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ], 'order_money')['data']
        ];

        $total_data = [
            'user_count' => $sysuser_model->getSysuserCount([]),
            'site_count' => $site_model->getSiteCount([])['data'],
            'order_count' => $sys_order_stat_model->getOrderCount([ ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ])['data'],
            'order_money' => $sys_order_stat_model->getOrderSum([ ['pay_status', '=', 1],['order_status', 'not in', [0,-1]] ], 'order_money')['data']
        ];

        $ratio_data = [
            'user_count' => diff_rate($today_data['user_count'], $yesterday_data['user_count']),
            'site_count' => diff_rate($today_data['site_count'], $yesterday_data['site_count']),
            'order_count' => diff_rate($today_data['order_count'], $yesterday_data['order_count']),
            'order_money' => diff_rate($today_data['order_money'], $yesterday_data['order_money'])
        ];

        $this->assign('today_data', $today_data);
        $this->assign('yesterday_data', $yesterday_data);
        $this->assign('total_data', $total_data);
        $this->assign('ratio_data', $ratio_data);

        $upgrade_model = new UpgradeModel();
        $contrast_res = $upgrade_model->certContrast();
        $is_same = (!empty($contrast_res['data'])) ? $contrast_res['data']['is_same'] : 0;
        $sync_status = $is_same == 1 ? 0 : 1;//是否需要更新证书
        $this->assign('sync_status', $sync_status);

		return $this->fetch('index/index');
	}
	
	/**
	 * 官网资讯
	 */
	public function news()
	{
		$web_model = new WebModel();
		$result = $web_model->news();
		return $result;
	}
}