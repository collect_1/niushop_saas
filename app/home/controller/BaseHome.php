<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\Controller;
use app\model\system\Menu;
use app\model\system\SysUser as SysUserModel;
use app\model\web\Config as ConfigModel;
use app\model\web\WebSite;

class BaseHome extends Controller
{
	protected $sys_uid;
	protected $sys_user_info;
	protected $url;
	protected $app_module = 'home';
	protected $addon = '';
	
	public function __construct()
	{
		//执行父类构造函数
		parent::__construct();

		request()->siteid(0);
		//检测基础登录
		$sys_user_model = new SysUserModel();
		$this->sys_uid = $sys_user_model->sys_uid($this->app_module);
		$this->url = request()->parseUrl();
		$this->addon = request()->addon() ? request()->addon() : '';
		$this->sys_user_info = $sys_user_model->sysUserInfo($this->app_module);
		$this->assign('sys_user_info', $this->sys_user_info);
		if (empty($this->sys_uid)) {
			$this->redirect(url("home/login/login"));
			exit();
		}
		$this->checkLogin();
		if (!request()->isAjax()) {
			//获取菜单
			$this->initBaseInfo();
		}
	}
	
	/**
	 * 加载基础信息
	 */
	private function initBaseInfo()
	{
		//加载网站基础信息
		$website = new WebSite();
		$website_info = $website->getWebSite([ [ 'site_id', '=', 0 ] ], 'title,logo,desc,keywords,web_status,close_reason,web_qrcode,web_phone');
		$this->assign("website_info", $website_info['data']);
		//加载版权信息
		$config_model = new ConfigModel();
		$copyright = $config_model->getCopyright();
		$this->assign('copyright', $copyright['data']['value']);
	}

	/**
	 * 验证登录
	 */
	private function checkLogin()
	{
		//验证基础登录
		if (!$this->sys_uid) {
			$this->redirect(url('home/login/login'));
		}
	}
	
	protected function forthMenu($params = [])
	{
		$url = strtolower($this->url);
		$menu_model = new Menu();
		$menu_info = $menu_model->getMenuInfo([ [ 'url', "=", $url ], [ 'level', '=', 4 ] ], 'parent');
		if (!empty($menu_info['data'])) {
			$menus = $menu_model->getMenuList([ [ 'app_module', "=", $this->app_module ], [ 'is_show', "=", 1 ], [ 'parent', '=', $menu_info['data']['parent'] ] ], '*', 'sort asc');
			foreach ($menus['data'] as $k => $v) {
				$menus['data'][ $k ]['parse_url'] = addon_url($menus['data'][ $k ]['url'], $params);
				if ($menus['data'][ $k ]['url'] == $url) {
					$menus['data'][ $k ]['selected'] = 1;
				} else {
					$menus['data'][ $k ]['selected'] = 0;
				}
			}
			$this->assign('forth_menu', $menus['data']);
		}
	}
	
}