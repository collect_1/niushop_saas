<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\system\SysOrder as SysOrderModel;

class Order extends Account
{
	
	/**
	 * 关闭订单
	 */
	public function close()
	{
		if (request()->isAjax()) {
			$order_id = input('order_id', 0);
			$order_model = new SysOrderModel();
			$condition = array(
				[ 'order_id', '=', $order_id ],
				[ 'buyer_uid', '=', $this->sys_uid ],
				[ 'order_status', 'not in', [ -1, 1 ] ],
			);
			$result = $order_model->orderClose($condition);
			return $result;
		}
	}
	
	/**
	 * 删除订单
	 * @return array
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$order_id = input('order_id', 0);
			$order_model = new SysOrderModel();
			$condition = array(
				[ 'order_id', '=', $order_id ],
				[ 'buyer_uid', '=', $this->sys_uid ],
				[ 'order_status', '=', -1 ],
			);
			$result = $order_model->orderDelete($condition);
			return $result;
		}
	}
	
}