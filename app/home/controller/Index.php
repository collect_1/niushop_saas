<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\system\Site as SiteModel;
use app\model\system\SiteConfig;
use think\facade\Cache;

use app\model\web\WebSite as WebsiteModel;

class Index extends BaseHome
{

    /**
     * 首页
     * @return mixed
     */
    public function index()
    {
        //平台配置信息
        $website_model = new WebsiteModel();
        $website_info = $website_model->getWebSite([ [ 'site_id', '=', 0 ] ], 'title,web_phone,web_qq,web_weixin,web_email,web_qrcode,logo');

        $this->assign('website_info', $website_info[ 'data' ]);

        if (request()->isAjax()) {
            $page_index = input('page', 1);

            $search_text = input('search_text', '');
            $condition = [
                [ 'sys_uid', '=', $this->sys_uid ]
            ];
            if (!empty($search_text)) {
                $condition[] = [ 'site_name', 'LIKE', "%{$search_text}%" ];
            }
            //获取当前系统会员的信息
            $site_model = new SiteModel();
            //获取当前系统用户的用户数据
            $shop_list = $site_model->getSitePageList($condition, $page_index, 20, '', '*');

            return $shop_list;
        } else {

            $this->assign('now', time());

            //体验开店设置
            $site_config = new SiteConfig();
            $config_info = $site_config->getOpenSiteConfig();

            //获取当前系统会员的信息
            $site_model = new SiteModel();
            $try_site_count = $site_model->getSiteCount([ [ 'is_try','=',1 ],[ 'sys_uid', '=', $this->sys_uid ] ]);
            $config_info[ 'data' ][ 'value' ]['site_count']=$try_site_count['data'];
            $this->assign('config_info', $config_info[ 'data' ][ 'value' ]);

            $cert = Cache::get("niushop_cert");
            $type = $cert[ 'type' ];

            $this->assign("business_type", $type);
            return $this->fetch("index/index");
        }

    }
}