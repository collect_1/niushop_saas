<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\system\Pay as PayModel;
use app\model\system\SysOrder as SysOrderModel;
use app\model\web\WebSite;

class Ordercreate extends BaseHome
{

	/**
	 * 订单创建
	 * @return mixed
	 */
	public function create()
	{
		if (request()->isAjax()) {
			$group_id = input('group_id', 0);//购买用户套餐数量
			$num = input('num', 0);//购买用户套餐数量
			$site_name = input('site_name', '');

			$sms_package_id = input('sms_package_id', 0);//短信套餐id
			$sms_package_num = input('sms_package_num', 1);//购买短信套餐数量

			$buyer_info = array(
				'buyer_uid' => $this->sys_uid,
				'buyer_name' => $this->sys_user_info['username'],
			);
			$data = array(
				'group_id' => $group_id,
				'site_name' => $site_name,
				'site_id' => 0,
				'group_num' => $num,
				'buyer_info' => $buyer_info,
				'sms_package_id' => $sms_package_id,
				'sms_package_num' => $sms_package_num,
			);
			$sys_order_model = new SysOrderModel();
			$res = $sys_order_model->orderCreate($data);
			return $res;
		}
	}

	/**
	 * 支付确认页
	 */
	public function confirm()
	{

	    $out_trade_no = input("out_trade_no", '');
		$sys_order_model = new SysOrderModel();
		if (request()->isAjax()) {

			$data = [
				'paying_money_certificate' => input('paying_money_certificate', ''),
				'paying_money_certificate_explain' => input('paying_money_certificate_explain', '')
			];
			$condition[] = [ 'out_trade_no', '=', $out_trade_no ];
			$condition[] = [ 'pay_status', '=', 0 ];
			return $sys_order_model->applyOfflinePay($condition, $data);

		} else {
			$detail_result = $sys_order_model->getOrderDetail($out_trade_no);
			$detail = $detail_result["data"];
			$this->assign("detail", $detail);
			//支付方式(支付给平台的钱  site_id为0)
			$pay = new PayModel();
			$pay_type = $pay->getPayType([ 'app_type' => 'pc', 'site_id' => 0 ]);
			$this->assign('pay_type', $pay_type['data']);
			$this->assign('pay_type_count', count($pay_type['data']));

			//收款账户
			$config_model = new WebSite();
			$receivable_config = $config_model->getSystemBankAccount();
			$this->assign('receivable_config', $receivable_config['data']['value']);
			return $this->fetch('ordercreate/confirm');
		}

	}

	/**
	 * 支订单付
	 */
	public function pay()
	{
		if (request()->isAjax()) {
			$pay_type = input('pay_type');
			$out_trade_no = input('out_trade_no');
			$app_type = 'pc';
			$pay_model = new PayModel();
			$return_url = addon_url("home/account/order");
			$result = $pay_model->pay($pay_type, $out_trade_no, $app_type, $this->sys_uid, $return_url);
			return $result;
		}
	}

	/**
	 * 验证是否支付
	 * @return \app\model\system\Ambigous
	 */
	public function verifyIsPay()
	{
		if (request()->isAjax()) {
			$out_trade_no = input('out_trade_no');
			$sys_order_model = new SysOrderModel();
			$order_info_result = $sys_order_model->getOrderInfo([ [ 'out_trade_no', '=', $out_trade_no ], [ 'order_status', '=', 1 ] ], 'order_status');
			if (!empty($order_info_result['data'])) {
				return $sys_order_model->success();
			} else {
				return $sys_order_model->error([], '支付未完成,请重新支付');
			}

		}
	}

	/**
	 * 二维码
	 */
	public function qrcode()
	{
		$param = input('param', '');
		return \extend\QRcode::png($param, false, '', 4, 1);
	}

}