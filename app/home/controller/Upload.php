<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\upload\Upload as UploadModel;

/**
 * 图片上传
 * Class Verify
 * @package app\shop\controller
 */
class Upload extends BaseHome
{
    protected $site_id = 0;

    public function __construct()
    {
        //执行父类构造函数
        parent::__construct();
    }

    /**
     * 上传(不存入相册)
     * @return \app\model\upload\Ambigous|\multitype
     */
    public function image()
    {
        $upload_model = new UploadModel();
        $thumb_type = input("thumb", "");
        $name = input("name", "");
        $watermark = input("watermark", 0); // 是否需生成水印
        $cloud = input("cloud", 1); // 是否需上传到云存储
        $param = array (
            "thumb_type" => "",
            "name" => "file",
            "watermark" => $watermark,
            "cloud" => $cloud
        );
        $path = $this->site_id > 0 ? "common/images/" . date("Ymd") . '/' : "common/images/" . date("Ymd") . '/';
        $result = $upload_model->setPath($path)->image($param);
        return $result;
    }

    /**
     * 上传(不存入相册)
     * @return \app\model\upload\Ambigous|\multitype
     */
    public function upload()
    {
        $upload_model = new UploadModel();
        $thumb_type = input("thumb", "");
        $name = input("name", "");
        $watermark = input("watermark", 0); // 是否需生成水印
        $cloud = input("cloud", 1); // 是否需上传到云存储
        $param = array (
            "thumb_type" => "",
            "name" => "file",
            "watermark" => $watermark,
            "cloud" => $cloud
        );
        $result = $upload_model->setPath("common/images/" . date("Ymd") . '/')->image($param);
        return $result;
    }
}