<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;

use app\model\system\Site as SiteModel;
use app\model\system\SiteConfig;
use app\model\system\SysUser;
use app\model\system\SiteGroup as SiteGroupModel;

class Site extends BaseHome
{
	
	/**
	 * 添加商城
	 */
	public function add()
	{
		$sys_user_model = new SysUser();
		$sys_user_info = $sys_user_model->getUserInfo([ [ 'sys_uid', '=', $this->sys_uid ] ]);
		$this->sys_user_info = $sys_user_info['data'];
		if (request()->isAjax()) {

            //店铺信息
            $site_data = [
                'site_name' => input('site_name', ''),//店铺名称
                'sys_uid' => $this->sys_uid,
                'is_try' => 0
            ];
			if ($this->sys_user_info['is_admin'] == 1) {

                $site_data['expire_time'] = 0;

			}else{
			    //查询是否开启体验
                $site_config = new SiteConfig();
                $config_info = $site_config->getOpenSiteConfig();
                if(empty($config_info['data']['value']) || $config_info['data']['value']['status'] == 0){
                    return error(-1, '非法请求');
                }else{
                    $site_data['group_id'] = $config_info['data']['value']['group_id'];
                    $site_data['expire_time'] = strtotime('+'.$config_info['data']['value']['day'].'day');
                    $site_data['is_try'] = 1;
                }
            }
			$site_model = new SiteModel();
			return $site_model->addSite($site_data);
		} else {
			return $this->fetch('site/add');
		}
	}
	
	/**
	 * 订单购买页
	 */
	public function payment()
	{
		$sys_user_model = new SysUser();
		$sys_user_info = $sys_user_model->getUserInfo([ [ 'sys_uid', '=', $this->sys_uid ] ]);
		$this->sys_user_info = $sys_user_info['data'];
		if ($this->sys_user_info['group_id'] <= 0)
			$this->error('请绑定用户套餐后再购买站点!');
		
		$group_info = $this->sys_user_info;
		$sys_user_group_model = new SiteGroupModel();
		$sys_group_info_result = $sys_user_group_model->getSiteGroupDetail([ [ 'group_id', '=', $this->sys_user_info['group_id'] ] ], '*');
		$group_info = $sys_group_info_result['data'];
		$create_site_array = $sys_group_info_result['data']['create_site_array'];
		array_multisort(array_column($create_site_array, 'num'), SORT_ASC, $create_site_array);//排序，根据num 排序
		$group_info['create_site_array'] = $create_site_array;
		$this->assign('info', $group_info);
		return $this->fetch('site/payment');
	}
}