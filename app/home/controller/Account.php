<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\home\controller;


use app\model\system\Addon;
use app\model\system\SysOrder as SysOrderModel;
use app\model\message\Sms;
use app\model\system\SysUser as SysUserModel;
use app\model\system\SysUser;
use app\model\web\WebSite as WebsiteModel;

class Account extends BaseHome
{
	
	public $menu = [];
	public $crumbs_view = [ [ 'title' => '个人中心', 'url' => 'home/account/account' ] ];
	
	public function __construct()
	{
		//执行父类构造函数
		parent::__construct();
		$this->menu = [
			[
				"title" => '账户安全',
				"is_select" => '0',
				"url" => 'home/account/account',
				'icon' => '',
				'select_icon' => '',
			],
			[
				"title" => '我的订购',
				"is_select" => '0',
				"url" => 'home/account/order',
				'icon' => '',
				'select_icon' => '',
				'child' => [
					[
						"title" => '订单详情',
						"is_select" => '0',
						"url" => 'home/account/orderdetail',
						'icon' => '',
						'select_icon' => '',
					],
				]
			],
			[
				"title" => '我的店铺',
				"is_select" => '0',
				"url" => 'home/index/index',
				'icon' => '',
				'select_icon' => '',
			],
		];
		$temp_url = strtolower($this->url);
		foreach ($this->menu as $k => $v) {
			if ($v['url'] == $temp_url) {
				$this->menu[ $k ]['is_select'] = 1;
				$this->crumbs_view[] = $v;
				break;
			} else {
				if (!empty($v['child'])) {
					foreach ($v['child'] as $child_k => $child_v) {
						if ($child_v['url'] == $temp_url) {
							$this->menu[ $k ]['is_select'] = 1;
							$this->crumbs_view[] = $v;
							$this->crumbs_view[] = $child_v;
							break 2;
						}
					}
				}
				$this->menu[ $k ]['is_select'] = 0;
			}
		}

		 //平台配置信息
        $website_model = new WebsiteModel();
        $website_info = $website_model->getWebSite([['site_id', '=', 0]], 'title,web_phone,web_qq,web_weixin,web_email,web_qrcode,logo');
        $this->assign('website_info',$website_info['data']);
        
		$this->assign('crumbs_view', $this->crumbs_view);
		$this->assign('crumbs_count', count($this->crumbs_view));
		$this->assign('menu', $this->menu);
	}
	
	/**
	 * 账户安全
	 * @return mixed
	 */
	public function account()
	{
		$sys_user_model = new SysUser();
		$sys_user_info = $sys_user_model->getUserInfo([ [ 'sys_uid', '=', $this->sys_uid ] ]);
		$this->assign('user_info', $sys_user_info['data']);
		return $this->fetch("account/account");
	}
	
	/**
	 * 我的订购
	 * @return mixed
	 */
	public function order()
	{
		$order_model = new SysOrderModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$condition = [
				[ 'is_delete', '=', 0 ]
			];
			$condition[] = [ 'buyer_uid', '=', $this->sys_uid ];
			$search_type = input('search_type', '');
			$search_text = input('search_text', '');
			if (!empty($search_text)) {
				$condition[] = [ $search_type, 'LIKE', '%' . $search_text . '%' ];
			}
			
			$order_type = input('order_type', '');
			if ($order_type) {
				$condition[] = [ 'order_type', '=', $order_type ];
			}
			
			$order_status = input('order_status', '');
			if ($order_status !== '') {
				$condition[] = [ 'order_status', '=', $order_status ];
			}
			
			$pay_type = input('pay_type', '');
			if ($pay_type) {
				$condition[] = [ 'pay_type', '=', $pay_type ];
			}
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && !$end_time) {
				$condition[] = [ 'create_time', '>=', date_to_time($start_time) ];
			} elseif (!$start_time && $end_time) {
				$condition[] = [ 'create_time', '<=', date_to_time($end_time) ];
			} elseif ($start_time && $end_time) {
				$condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
			}
			
			$list = $order_model->getSysOrderPageList($condition, $page, $page_size, '*', "create_time desc");
			return $list;
		} else {
			$this->assign('order_status', $order_model->order_status);
			$this->assign('order_type', $order_model->order_type);
			$this->assign('pay_type', $order_model->getPayType());
			return $this->fetch("account/order");
		}
		
	}
	
	/**
	 * 订单详情
	 */
	public function orderdetail()
	{
		$order_id = input('order_id', 0);
		$order_model = new SysOrderModel();
		$condition = array(
			[ 'order_id', '=', $order_id ],
			[ 'buyer_uid', '=', $this->sys_uid ],
		);
		$detail_result = $order_model->getOrderInfo($condition);
		$detail = $detail_result["data"];
		$this->assign("detail", $detail);
		return $this->fetch("account/order_detail");
	}
}