<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace app\event;

use app\model\system\SysOrder;

/**
 * 系统订单自动关闭
 */
class CronSysOrderClose
{
    // 行为扩展的执行入口必须是run
    public function handle($data)
    {
        $order_model = new  SysOrder();
        $condition = array(
            ['order_status', '=', 0],
            ['order_id', '=', $data['order_id']]
        );
        $result = $order_model->orderClose($condition);
        return $result;
    }

}