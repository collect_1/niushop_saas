<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\index\controller;

use app\Controller;
use app\model\web\Config as ConfigModel;
use think\facade\Cache;
use app\model\web\Notice as NoticeModel;
use app\model\web\Help as HelpModel;
use app\model\web\WebSite as WebsiteModel;
use app\model\system\Addon;
use app\model\system\SiteGroup;

class Index extends Controller
{
    public $url;
    public $menu = [];

    public function __construct()
    {
        $this->url = request()->parseUrl();
        $this->menu = [
            [
                "title" => '首页',
                "is_select" => '1',
                "url" => 'index/index/index',
                'icon' => '',
                'select_icon' => '',
            ],
            [
                "title" => '价格',
                "is_select" => '0',
                "url" => 'index/index/price',
                'icon' => '',
                'select_icon' => '',
            ],
            [
                "title" => '关于我们',
                "is_select" => '0',
                "url" => 'index/index/about',
                'icon' => '',
                'select_icon' => '',
            ],
        ];
        $temp_url = strtolower($this->url);
        foreach ($this->menu as $k => $v) {
            if ($v[ 'url' ] == $temp_url || $temp_url == '/index/index') {
                $this->menu[ $k ][ 'is_select' ] = 1;
                break;
            } else {
                $this->menu[ $k ][ 'is_select' ] = 0;
            }
        }

        //网站公告
        $notice_model = new NoticeModel();
        $notice_list = $notice_model->getNoticePageList([ [ 'site_id', '=', 0 ] ], 1, 5, 'is_top desc,create_time desc', 'id,title');
        $this->assign('notice_list', $notice_list[ 'data' ][ 'list' ]);

        //入驻帮助
        $help_model = new HelpModel();
        $help_condition = [
            [ 'site_id', '=', 0 ]
        ];
        $order = 'create_time desc';
        $help_list = $help_model->getHelpPageList($help_condition, 1, 5, $order);
        $this->assign("help_list", $help_list[ 'data' ][ 'list' ]);
        $this->assign('menu', $this->menu);

        $cert = Cache::get("niushop_cert");
        $this->assign("business_type", $cert[ 'type' ]);

        //平台配置信息
        $website_model = new WebsiteModel();
        $website_info = $website_model->getWebSite([ [ 'site_id', '=', 0 ] ], 'title,web_phone,web_qq,web_weixin,web_email,web_qrcode,logo');
        $this->assign('website_info', $website_info[ 'data' ]);

        //加载版权信息
        $config_model = new ConfigModel();
        $copyright = $config_model->getCopyright();
        $this->assign('copyright', $copyright[ 'data' ][ 'value' ]);

        $root_url = __ROOT__;
        $this->assign("root_url", $root_url);
    }

    /**
     * 首页
     * @return mixed
     */
    public function index()
    {

        $cert = Cache::get("niushop_cert");
        $this->assign("business_type", $cert[ 'type' ]);

        $this->assign("menu_info", [ 'title' => "首页" ]);
        return $this->fetch("index/index");
    }

    /**
     * 价格
     * @return mixed
     */
    public function price()
    {
        $condition = [ [ 'status', '=', 1 ] ];

        $addon = new Addon();
        $install_result = $addon->getAddonList([ [ "type", "<>", "system" ] ], "name,title");
        $install = $install_result[ "data" ];
        $install_list = array_column($install, "title", "name");
        $sys_user_group_model = new SiteGroup();
        $list = $sys_user_group_model->getSiteGroupList($condition, '', "fee asc");
        if (!empty($list[ "data" ])) {
            foreach ($list[ "data" ] as $k => $v) {
                $temp_array = explode(",", $v[ 'addon_array' ]);
                $exist_addon_array = [];
                $noexist_addon_array = [];
                foreach ($install_list as $temp_k => $temp_v) {
                    if (in_array($temp_k, $temp_array)) {
                        $exist_addon_array[] = $temp_v;
                    } else {
                        $noexist_addon_array[] = $temp_v;
                    }
                }
                $list[ "data" ][ $k ][ "exist_addon_list" ] = $exist_addon_array;
                $list[ "data" ][ $k ][ "noexist_addon_list" ] = $noexist_addon_array;
            }
        }
        $this->assign('addon_list', $list[ 'data' ]);

        $cert = Cache::get("niushop_cert");
        $this->assign("business_type", $cert[ 'type' ]);

        $this->assign("menu_info", [ 'title' => "价格" ]);
        return $this->fetch("index/price");
    }

    /**
     * 关于我们
     * @return mixed
     */
    public function about()
    {
        $cert = Cache::get("niushop_cert");
        $this->assign("business_type", $cert[ 'type' ]);
        $this->assign("menu_info", [ 'title' => "关于我们" ]);
        return $this->fetch("index/about");
    }

    /**
     *  公告续签
     */
    public function getNoticeDetail()
    {
        $id = input('id', 0);
        $notice = new NoticeModel();
        $info = $notice->getNoticeInfo([ [ 'id', '=', $id ] ]);
        $this->assign('info', $info[ 'data' ]);
        $this->assign("menu_info", [ 'title' => "公告" ]);
        return $this->fetch('index/notice_detail');
    }

    /**
     * 帮助详情
     */
    public function getHelpDetail()
    {
        $id = input('id', 0);
        $help_model = new HelpModel();
        $help_info = $help_model->getHelpInfo($id);
        $this->assign('help_info', $help_info[ 'data' ]);
        $this->assign("menu_info", [ 'title' => "站点帮助" ]);
        return $this->fetch('index/help_detail');
    }
}