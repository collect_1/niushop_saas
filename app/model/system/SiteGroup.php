<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace app\model\system;

use app\model\BaseModel;
use think\facade\Cache;

/**
 * 店铺套餐
 * @author Administrator
 *
 */
class SiteGroup extends BaseModel
{

	/**
	 * getSiteGroupInfo 详情
	 * @param $condtion
	 * @param string $fields
	 */
	public function getSiteGroupInfo($condtion, $fields = '*')
	{

		$res = model('site_group')->getInfo($condtion, $fields);
		return $this->success($res);
	}

	/**
	 * getSiteGroupDetail 详情
	 * @param $condtion
	 * @param string $fields
	 */
	public function getSiteGroupDetail($condtion, $fields = '*')
	{

		$res = model('site_group')->getInfo($condtion, $fields);
		return $this->success($res);

	}

	/**
	 * 添加
	 */
	public function addSiteGroup($data)
	{
		$count = model('site_group')->getCount([ [ 'group_name', '=', $data[ 'group_name' ] ] ]);
		if ($count > 0) {
			return $this->error('-1', '该套餐名称已经存在!');
		}
		$data[ 'create_time' ] = time();
		$res = model('site_group')->add($data);
		return $this->success($res);
	}

	/**
	 * 多个添加
	 */
	public function addSiteGroupList($data)
	{
		foreach ($data as $k => $v) {
			$count = model('site_group')->getCount([ [ 'group_name', '=', $v[ 'group_name' ] ] ]);
			if ($count > 0) {
				return $this->error('-1', '该套餐名称已经存在!');
			}
			$data[ $k ][ 'create_time' ] = time();
		}
		$res = model('site_group')->addList($data);
		return $this->success($res);
	}

	/**
	 * 修改，只能单个修改
	 * @param $site_data
	 * @param $condition
	 * @return int
	 */
	public function editSiteGroup($data, $condition)
	{
		$check_data = array_column($condition, 2, 0);
		if (empty($check_data[ 'group_id' ])) {
			return $this->error(-1, '参数错误');
		}
		$group_id = $check_data[ 'group_id' ];
		$count = model('site_group')->getCount([ [ 'group_id', '<>', $group_id ], [ 'group_name', '=', $data[ 'group_name' ] ] ]);
		if ($count > 0) {
			return $this->error(-1, '该套餐名称已经存在!');
		}
		$data[ 'modify_time' ] = time();
		$res = model('site_group')->update($data, $condition);
		if ($res) {
			//修改对应店铺信息
			$data_site = [
				'site_addon' => $data[ 'addon_array' ],
				'group_name' => $data[ 'group_name' ]
			];
			model("site")->update($data_site, [ [ 'group_id', '=', $group_id ] ]);
		}
		return $this->success($res);
	}

	/**
	 * 获取列表
	 */
	public function getSiteGroupPageList($condition, $page, $page_size, $order = "create_time desc", $fields = '*')
	{
		$res = model('site_group')->pageList($condition, $fields, $order, $page, $page_size);
		return $this->success($res);
	}

	/**
	 * 获取列表
	 */
	public function getSiteGroupList($condition, $fields = '*', $order = "create_time desc")
	{
		$res = model('site_group')->getList($condition, $fields, $order);
		return $this->success($res);
	}

	/**
	 * 删除
	 * @param $condition
	 */
	public function deleteSiteGroup($group_id)
	{
		//开店套餐删除之前要考虑店铺 - 订单
		$site_count = model('site')->getCount([ [ 'group_id', '=', $group_id ] ]);
		if ($site_count > 0) {
			return $this->error('-1', '该套餐存在店铺，不得删除！');
		}

		$order_count = model('sys_order')->getCount([ [ 'group_id', '=', $group_id ], [ 'pay_status', '=', 0 ] ]);
		if ($order_count > 0) {
			return $this->error('-1', '该套餐存在未支付订单，不得删除！');
		}

		$res = model('site_group')->delete([ [ 'group_id', '=', $group_id ] ]);
		return $this->success($res);
	}


	/**********************************************************************  订单相关 start *********************************************************************************/

	/**
	 * 订单完成
	 */
	public function orderPay($order_info)
	{
		$result = $this->orderSuccess($order_info);
		return $result;
	}


	/**
	 * 套餐订单完成
	 */
	public function orderSuccess($order_info)
	{

		if ($order_info[ 'order_type' ] == 'group') {

			if ($order_info[ 'site_id' ] == 0) {
				//创建店铺
				$site_data = [
					'site_name' => $order_info[ 'site_name' ],//店铺名称
					'sys_uid' => $order_info[ 'buyer_uid' ],
					'is_try' => 0,
					'group_id' => $order_info[ 'group_id' ],
					'expire_time' => strtotime('+' . $order_info[ 'group_num' ] . $order_info[ 'group_period_unit' ])
				];
				$site_model = new Site();
				$res = $site_model->addSite($site_data);
				if ($res[ 'code' ] >= 0) {
					model('sys_order')->update([ 'site_id' => $res[ 'data' ]], [ [ 'order_id', '=', $order_info[ 'order_id' ] ] ]);
				}
				return $res;

			} else {

				//已经有店铺
				$site_info = model("site")->getInfo([ [ 'site_id', '=', $order_info[ 'site_id' ] ] ], '*');
				if ($site_info[ 'is_try' ] == 1) {
					$cache = Cache::get("niushop_cert");
					if (!empty($cache)) {
						if ($cache[ 'site_num' ] != 0) {
							$site_count = model("site")->getCount([ [ 'site_type', '=', 'shop' ], [ 'is_try', '=', 0 ] ]);
							if ($site_count >= $cache[ 'site_num' ]) {
								return $this->error(-1, "当前系统授权最大店铺数量".$cache[ 'site_num' ]."，请联系管理员购买");
							}
						}

					}
					//体验店铺
					$end_time = strtotime('+' . $order_info[ 'group_num' ] . $order_info[ 'group_period_unit' ], time());

				} else {

					//站点续期
					$now = time();
					if ($now > $site_info[ 'expire_time' ]) {
						//站点已经过期
						$end_time = strtotime('+' . $order_info[ 'group_num' ] . $order_info[ 'group_period_unit' ]);

					} else {
						//站点未过期
						$end_time = strtotime('+' . $order_info[ 'group_num' ] . $order_info[ 'group_period_unit' ], $site_info[ 'expire_time' ]);
					}
				}

                model('sys_order')->update(['is_renew' => 1] , [ [ 'order_id', '=', $order_info[ 'order_id' ] ] ]);

				//站点变化
				$site_group_model = new SiteGroup();
				$group_info = $site_group_model->getSiteGroupInfo([ [ 'group_id', '=', $order_info[ 'group_id' ] ] ], 'group_id, group_name, addon_array,sms_num');

				if ($site_info[ 'is_try' ] == 1) {
					$sms_num = $group_info[ 'data' ][ 'sms_num' ];
				} else {
					$sms_num = $site_info[ 'sms_num' ];
				}
				$data = [
					'group_id' => $group_info[ 'data' ][ 'group_id' ],
					'group_name' => $group_info[ 'data' ][ 'group_name' ],
					'site_addon' => $group_info[ 'data' ][ 'addon_array' ],
					'expire_time' => $end_time,
					'sms_num' => $sms_num,
					'is_try' => 0
				];
				$site_model = new Site();
				$res = $site_model->editSite($data, [ [ 'site_id', '=', $order_info[ 'site_id' ] ] ]);

				return $res;
			}
		}
	}


	/**********************************************************************  订单相关 end *********************************************************************************/

}