<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;

use app\model\BaseModel;

/**
 * 店铺套餐
 * @author Administrator
 *
 */
class SiteConfig extends BaseModel
{
	
	/**
	 * 开店试用设置
	 * @param $data
	 * @return array
	 */
	public function setOpenSiteConfig($data)
	{
		$config = new Config();
		$res = $config->setConfig($data, '体验开店设置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'OPEN_SITE_CONFIG' ] ]);
		return $this->success($res);
	}
	
	/**
	 * 获取开店试用设置
	 * @return array
	 */
	public function getOpenSiteConfig()
	{
		$config = new Config();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'OPEN_SITE_CONFIG' ] ]);
		if (empty($res['data']['value'])) {
			$res['data']['value'] = [
				'status' => 0,
				'day' => 0,
				'group_id' => 0,
				'site_num' => 10,
                'sms_num' => 30
			];
		}
		return $res;
	}
}