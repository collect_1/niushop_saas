<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\web;

use think\facade\Cache;
use app\model\BaseModel;
use app\model\system\Config as ConfigModel;

/**
 * 系统站点信息管理
 * @author Administrator
 *
 */
class WebSite extends BaseModel
{

    /**
     * 获取站点信息
     * @param array $condition
     * @param string $field
     */
    public function getWebSite($condition, $field = '*')
    {
        $data = json_encode([ $condition, $field ]);
        $cache = Cache::get("weisite_getWebSite_" . $data);
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $res = model('website')->getInfo($condition, $field);
        if (empty($res)) {
            $check_condition = array_column($condition, 2, 0);
            //初始化站点信息
            if ($check_condition[ 'site_id' ] == 0) {
                model('website')->add([ 'site_id' => $check_condition[ 'site_id' ] ]);
                $res = model('website')->getInfo($condition, $field);
            }
        }
        Cache::tag("website")->set("weisite_getWebSite_" . $data, $res);
        return $this->success($res);
    }

    /**
     * 设置站点信息
     * @param unknown $data
     */
    public function setWebSite($data, $condition)
    {
        $website_count = model('website')->getCount($condition);
        if ($website_count == 0) {
            $data[ 'create_time' ] = time();
            $res = model('website')->add($data);
        } else {
            $data[ 'modify_time' ] = time();
            $res = model('website')->update($data, $condition);
        }
        Cache::tag("website")->clear();
        return $this->success($res);
    }

    /**
     * 获取系统银行账户
     */
    public function getSystemBankAccount()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 1 ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'SYSTEM_BANK_ACCOUNT' ] ]);
        if (empty($res[ 'data' ][ 'value' ])) {
            $res[ 'data' ][ 'value' ] = [
                'bank_account_name' => '',
                'bank_account_no' => '',
                'bank_name' => '',
                'bank_address' => ''
            ];
        }
        return $res;
    }

    /**
     * 设置系统银行账户
     * @param unknown $data
     * @return Ambigous <multitype:unknown , multitype:number unknown >
     */
    public function setSystemBankAccount($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '平台银行账户', 1, [ [ 'site_id', '=', 1 ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'SYSTEM_BANK_ACCOUNT' ] ]);
        return $res;
    }

    /**
     * 获取发票设置
     */
    public function getInvoiceConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 1 ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'SYSTEM_INVOICE_CONFIG' ] ]);
        if (empty($res[ 'data' ][ 'value' ])) {
            $res[ 'data' ][ 'value' ] = [
                'status' => 0,
                'rate' => 0,
                'content' => '',
                'money' => 0
            ];
        }
        return $res;
    }

    /**
     * 设置发票设置
     * @param unknown $data
     * @return Ambigous <multitype:unknown , multitype:number unknown >
     */
    public function setInvoiceConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '发票设置', 1, [ [ 'site_id', '=', 1 ], [ 'app_module', '=', 'shop' ], [ 'config_key', '=', 'SYSTEM_INVOICE_CONFIG' ] ]);
        return $res;
    }

}