<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\system\Site as SiteModel;
use app\model\shop\Shop as ShopModel;

/**
 * 店铺
 * @author Administrator
 *
 */
class Site extends BaseApi
{

    /**
     * 基础信息
     */
    public function info()
    {
        $field = 'site_id,site_domain,group_name,expire_time,site_name,logo';
        $website_model = new SiteModel();
        $info = $website_model->getSiteInfo([ [ 'site_id', '=', $this->site_id ] ], $field);
        if (!empty($info[ 'data' ])) {

            $time = time();
            if ($info[ 'data' ][ 'expire_time' ] == 0) {
                $shop_status = 1;//正常
            } elseif ($info[ 'data' ][ 'expire_time' ] > $time) {
                $cha = $info[ 'data' ][ 'expire_time' ] - $time;
                $date = ceil(( $cha / 86400 ));
                if ($date < 30) {
                    $shop_status = 2;//即将到期
                } else {
                    $shop_status = 1;//正常
                }
            } else {
                $shop_status = -1;//关闭
            }
            $info[ 'data' ][ 'shop_status' ] = $shop_status;
        }
        return $this->response($info);
    }
    
    /**
     * 手机端二维码
     * @return false|string
     */
    public function wapQrcode()
    {
        $shop_model = new ShopModel();
        $res = $shop_model->qrcode($this->site_id);
        return $this->response($res);
    }

    /**
     * 是否显示店铺相关功能，用于审核小程序
     */
    public function isShow()
    {
        $res = 1;// 0 隐藏，1 显示
        return $this->response($this->success($res));
    }

    /**
     * 店铺状态
     * @return false|string
     */
    public function status(){
        return $this->response($this->success());
    }
}